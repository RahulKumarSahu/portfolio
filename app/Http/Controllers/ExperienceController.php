<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use Illuminate\Http\Request;

class ExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experiences = Experience::get();
        return view('dashboard.exprience.index', compact('experiences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.exprience.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $experiences = new Experience();
        // dd($experiences);
        // $experiences->id = $request->id;
        $experiences->title = $request->title;
        $experiences->description = $request->description;
        $experiences->save();
        return redirect('experiences');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $experiences = Experience::find($id);
        // dd($experiences);
        return view('dashboard.exprience.show', compact('experiences'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $experiences = Experience::find($id);
        return view('dashboard.exprience.edit', compact('experiences'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $experiences = Experience::find($id);

        // $experiences->id = $request->id;
        $experiences->title = $request->title;
        $experiences->description = $request->description;
        $experiences->update();
        return redirect('experiences');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $experiences = Experience::find($id);
        $experiences->delete();
        return redirect('experiences');
    }


}