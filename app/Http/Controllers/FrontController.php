<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use App\Models\Project;
use App\Models\Skill;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $experiences = Experience::get();
        // dump($experiences);
        $skills = Skill::get();
        $projects = Project::get();
        // dd($skills);
        // return view('layouts.portfolio.index', compact('experiences', 'skills'));
        return view('dashboard.portfolio.index', compact('experiences', 'skills', 'projects'));
    }

    public function port_project()
    {
        return view('dashboard.portfolio.work');
    }
}