<?php

namespace App\Observers;

use App\Models\Project;
use Illuminate\Support\Facades\File;

class ProjectObserver
{
    public function creating(Project $Project)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $Project->photo = $file_name; // Save file name to database
        }
    }

    /**
     * Handle the Project "created" event.
     *
     * @param  \App\Models\Project  $Project
     * @return void
     */
    public function created(Project $Project)
    {
        if (request()->file) {
            $path = public_path('project-uploads/projects');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $Project->photo);
        }
    }

    public function updating(Project $Project)
    {
        if (request()->file) {

            // Old file delete code
            $path = public_path('project-uploads/projects/');
            $this->deleteFile($path . $Project->photo);

            $file_name = time() . '.' . request()->file->extension();
            $Project->photo = $file_name; // Save file name to database
        }
    }

    /**
     * Handle the Project "updated" event.
     *
     * @param  \App\Models\Project  $Project
     * @return void
     */
    public function updated(Project $Project)
    {
        if (request()->file) {

            $path = public_path('project-uploads/projects/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $Project->photo);
        }
    }

    /**
     * Handle the Project "deleted" event.
     *
     * @param  \App\Models\Project  $Project
     * @return void
     */
    public function deleted(Project $Project)
    {
        $path = public_path('project-uploads/projects/');

        // Old file delete code
        $this->deleteFile($path . $Project->photo);
    }

    /**
     * Handle the Project "restored" event.
     *
     * @param  \App\Models\Project  $Project
     * @return void
     */
    public function restored(Project $Project)
    {
        //
    }

    /**
     * Handle the Project "force deleted" event.
     *
     * @param  \App\Models\Project  $Project
     * @return void
     */
    public function forceDeleted(Project $Project)
    {
        //
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}