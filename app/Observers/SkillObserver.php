<?php

namespace App\Observers;

use App\Models\Skill;
use Illuminate\Support\Facades\File;


class SkillObserver
{

    public function creating(Skill $skill)
    {
        if(request()->file)
        {
            $file_name = time().'.'.request()->file->extension();
            $skill->image = $file_name; // Save file name to database
        }
    }

    public function created(Skill $skill)
    {
        if(request()->file)
        {
            $path = public_path('skill_uploads/skills/');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $skill->image);
        }
    }

    /**
     * Handle the Skill "updated" event.
     *
     * @param  \App\Models\Skill  $skill
     * @return void
     */
    public function updating(Skill $skill)
    {
        if(request()->file) {

            // Old file delete code
            $path = public_path('skill_uploads/skills/');
            $this->deleteFile($path.$skill->image);

            $file_name = time().'.'.request()->file->extension();
            $skill->image = $file_name; // Save file name to database
        }
    }


    public function updated(Skill $skill)
    {
        if(request()->file) {

            $path = public_path('skill_uploads/skills/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $skill->image);
        }
    }

    /**
     * Handle the Skill "deleted" event.
     *
     * @param  \App\Models\Skill  $skill
     * @return void
     */
    public function deleted(Skill $skill)
    {
        $path = public_path('skill_uploads/skills/');

        // Old file delete code
        $this->deleteFile($path.$skill->image);
    }

    /**
     * Handle the Skill "restored" event.
     *
     * @param  \App\Models\Skill  $skill
     * @return void
     */
    public function restored(Skill $skill)
    {
        //
    }

    /**
     * Handle the Skill "force deleted" event.
     *
     * @param  \App\Models\Skill  $skill
     * @return void
     */
    public function forceDeleted(Skill $skill)
    {
        //
    }
    public function deleteFile($path)
    {
        File::delete($path);
    }
}