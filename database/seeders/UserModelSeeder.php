<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@example.com';
        $user->gender = 'male';
        $user->contact_no = '9088765412';
        $user->password = Hash::make('123456');
        $user->status = 'active';
        $user->save();
    }
}
