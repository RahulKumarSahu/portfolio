{{-- @section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('experiences.create') }}"> <button type="button"
                        class="btn btn-outline-primary mt-3 mb-4">Create</button> </a>
                <table class="table table-bordered bordered primary">
                    <thead>
                        <tr>
                            <th scope="col">Subject</th>
                            <th scope="col">Detail</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            @foreach ($experiences as $item)
                                <th>
                                    <article class="timeline-entry animate-box" data-animate-effect="fadeInRight">
                                        <div class="timeline-entry-inner">
                                            <div class="timeline-icon color-1">
                                                <i class="icon-pen2"></i>
                                            </div>
                                            <div class="timeline-label">
                                                <h2><a href="#">{{ $item->subject }}</a>
                                                    <span>2017-2018</span>
                                                </h2>
                                            </div>
                                </th>
                                <th>

            </th>
            <th>
                <a href="{{ route('', ) }}"><button type="button"
                        class="btn btn-outline-primary">Show</button></a>
                <form action="{{ route('experiences.destroy', $item->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                </form>
                <a href="{{ route('experiences.edit', $item->id) }}"><button type="button"
                        class="btn btn-outline-success">Update</button></a>
            </th>
            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    </div>
    </div>

@endsection --}}

@extends('layouts.dashboard')

@section('title')
    Users
@endsection

@section('css')
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">

        </div>
        <div class="col-lg-2">
            <a href="{{ route('experiences.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add User</a>
        </div>
    </div>
@endsection

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Users List
                        </h5>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover users-table">
                                <thead>
                                    <tr>
                                        <th>Subject</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($experiences as $item)
                                        <tr>
                                            <td>
                                                <article class="timeline-entry animate-box"
                                                    data-animate-effect="fadeInRight">
                                                    <div class="timeline-entry-inner">
                                                        <div class="timeline-icon color-1">
                                                            <i class="icon-pen2"></i>
                                                        </div>
                                                        <div class="timeline-label">
                                                            <h2><a href="#">{{ $item->title }}</a>
                                                                <span>2017-2018</span>
                                                            </h2>
                                                        </div>
                                            </td>
                                            <td>
                                                <p>{{ $item->description }}</p>
                        </div>
                        </article>
                        </td>


                        <td>
                            <div class="btn-group">
                                <a href="{{ route('experiences.show', $item->id) }}" class="btn-white btn btn-xs">View</a>

                                <a href="{{ route('experiences.edit', $item->id) }}" class="btn-white btn btn-xs">Edit</a>
                                {{-- route('experiences.destroy', $item->id) --}}
                                {{-- <a href="javascript:;" data-user-id="{{ $item->id }}"
                                    class="btn-white btn btn-xs delete-user">Delete</a> --}}
                                    <form action="{{ route('experiences.destroy', $item->id) }}" method="POST">
                                    @csrf
                                     @method('DELETE')
                                        <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                     </form>
                            </div>
                        </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.users-table').DataTable();

            @if (session('success'))
                toastr.success('{{ session('success') }}', 'Success');
            @endif

            $('.delete-user').click(function() {

                let userId = $(this).data('user-id');

                // Generate Token method 1
                // let token = $("meta[name='csrf-token']").attr("content");

                // Generate Token method 2
                let token = '{{ csrf_token() }}';

                let url = '{{ route('users.destroy', ':id') }}';
                url = url.replace(':id', userId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function() {

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',

                        },
                        success: function() {
                            swal("Deleted!", "User account has been deleted.",
                                "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });

                });
            });

        });
    </script>
@endsection
