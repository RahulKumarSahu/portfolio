@extends('layouts/portfolio')

@section('sidebar')

    <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle" data-toggle="collapse" data-target="#navbar"
        aria-expanded="false" aria-controls="navbar"><i></i></a>
    <aside id="colorlib-aside" role="complementary" class="border js-fullheight">
        <div class="text-center">
            <div class="author-img" style="background-image: url({{ asset('assets/images/profile.jpg') }} ">
            </div>
            <h1 id="colorlib-logo"><a href="index.html">Rahul Kumar Sahu</a></h1>
            <span class="position">Full Stack Developer in Mahasamund</span>
        </div>
        <nav id="colorlib-main-menu" role="navigation" class="navbar">
            <div id="navbar" class="collapse">
                <ul>
                    <li class="active"><a href="#" data-nav-section="home">Home</a></li>
                    <li><a href="#" data-nav-section="about">About</a></li>
                    <li><a href="#" data-nav-section="skills">Skills</a></li>
                    <li><a href="#" data-nav-section="education">Education</a></li>
                    <li><a href="#" data-nav-section="experience">Experience</a></li>
                    <li><a href="#" data-nav-section="work">Project</a></li>
                    <li><a href="#" data-nav-section="contact">Contact</a></li>

                </ul>
            </div>
        </nav>
        <div class="colorlib-footer">
            <p><small>©
                    <script>
                        document.write(new Date().getFullYear());
                    </script>2021 All rights reserved. Made with <i class="icon-heart" aria-hidden="true"></i> by <a
                        href="https://colorlib.com" target="_blank">Colorlib</a>
                    <span>Distributed by <a href="https://themewagon.com" target="_blank">ThemeWagon</a></span>
                    <span>Demo Images: <a href="https://unsplash.com/" target="_blank">Unsplash.com</a></span></small></p>
            <ul>
                <li><a href="https://github.com/rsahu93"><i class="icon-github"></i></a></li>
                <li><a href="https://www.facebook.com/profile.php?id=100011730480437"><i class="icon-facebook2"></i></a>
                </li>
                <li><a href="#"><i class="icon-twitter2"></i></a></li>
                <li><a href="#"><i class="icon-instagram"></i></a></li>
                <li><a href="#"><i class="icon-linkedin2"></i></a></li>
            </ul>

        </div>
    </aside>

@endsection

@section('main')

    <section id="colorlib-hero" class="js-fullheight" data-section="home">
        <div class="flexslider js-fullheight">
            <ul class="slides">
                <li id="image" style="background-image: url( {{ asset('assets/images/img_bg_2.jpg') }} );">
                    <div class="overlay"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div
                                class="col-md-6 col-md-offset-3 col-md-pull-3 col-sm-12 col-xs-12 js-fullheight slider-text">
                                <div class="slider-text-inner js-fullheight">
                                    <div class="desc">
                                        <h1>Hi! <br>I'm Rahul Kumar Sahu</h1>
                                        <p><a href="https://drive.google.com/drive/u/2/folders/1A2pu_SjOfmo6JisB0C_PaiCSf11x3jIJ"
                                                target="_blank" class="btn btn-primary btn-learn">Download CV
                                                <i class="icon-download4"></i></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
    </section>

    {{-- about me --}}
    <section class="colorlib-about" data-section="about">
        <div class="colorlib-narrow-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row row-bottom-padded-sm animate-box" data-animate-effect="fadeInLeft">
                        <div class="col-md-12">
                            <div class="about-desc">
                                <span class="heading-meta">About Us</span>
                                <h2 class="colorlib-heading">Who Am I?</h2>
                                <p><strong>Hi I'm Rahul Kumar Sahu</strong> full stack developent from
                                    mahasamund chhattisgarh. I like to take challenge that I can do it.
                                    though I'm fresher now but i need opportunity to show my skills. </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
                            <div class="hire">
                                <h2>I am happy to know you <br>that 3+ projects done sucessfully!</h2>
                                <a href="https://drive.google.com/drive/u/2/folders/1A2pu_SjOfmo6JisB0C_PaiCSf11x3jIJ"
                                target="_blank"" class="btn-hire">Hire me</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- skill --}}
    <section class="colorlib-skills" data-section="skills">
        <div class="colorlib-narrow-content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                    <span class="heading-meta">My Specialty</span>
                    <h2 class="colorlib-heading animate-box">My Skills</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
                    <p>I create custome website to help business do better online. I have knowledge of html5
                        ,css3 ,bootstrap ,javascript ,jquery and Laravel.</p>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-6 animate-box">
                        @foreach ($skills as $skill)
                    <div class="col-md-12 col-sm-6 animate-box card2">
                         <div class="card-body">
                             <img src="{{ asset('skill_uploads/skills/'.$skill->image) }}" alt="" class="tech_img">
                         </div>
                        <div class="card-footer">
                            <p class="paragraph">{{ $skill->name }}</p>
                        </div>
                    </div>
                        @endforeach


                    </div>
                </div>

            </div>
        </div>
    </section>
    {{-- education --}}
    <section class="colorlib-education" data-section="education">
        <div class="colorlib-narrow-content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                    <span class="heading-meta">Education</span>
                    <h2 class="colorlib-heading animate-box">Education</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
                    <div class="fancy-collapse-panel">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                            href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Bachelor
                                            Degree of Computer Science
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                    aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="panel-body">
                                            <large>Chhattisgarh Swami Vivekanand Technical University,
                                                Bhilai</large><br>
                                            <small> M M College Of Technology,Raipr(C.G)</small> <small> 2016-2020</small><br>
                                            <small>Percentage- 74.91 %</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">High
                                                School Secondary Education
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                                        aria-labelledby="headingFive">
                                        <div class="panel-body">
                                            <p> Govt.Higher Secondary School, Sirpur(C.G)</p>
                                            <small> From 9th to 12th</small>
                                            <small> 2013-2016</small>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
    </section>
    {{-- experience --}}
    <section class="colorlib-experience" data-section="experience">
        <div class="colorlib-narrow-content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                    <span class="heading-meta">Experience</span>
                    <h2 class="colorlib-heading animate-box">Work Experience</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="timeline-centered">
                        @foreach ($experiences as $item)
                            <article class="timeline-entry animate-box" data-animate-effect="fadeInRight">
                                <div class="timeline-entry-inner">
                                    <div class="timeline-icon color-1">
                                        <i class="icon-pen2"></i>
                                    </div>
                                    <div class="timeline-label">
                                        <h2><a href="#">{{ $item->title }}</a>
                                            <span>2020-2021</span>
                                        </h2>

                                        <p>{{ $item->description }}</p>
                                    </div>
                                </div>
                            </article>
                        @endforeach
                        <article class="timeline-entry begin animate-box" data-animate-effect="fadeInBottom">
                            <div class="timeline-entry-inner">
                                <div class="timeline-icon color-none">
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- work --}}
    <section class="colorlib-work" data-section="work">
        <div class="colorlib-narrow-content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                    <span class="heading-meta">My Project</span>
                    <h2 class="colorlib-heading animate-box">Recent Project</h2>
                </div>
            </div>
            <div class="row row-bottom-padded-sm animate-box" data-animate-effect="fadeInLeft">
                <div class="col-md-12">
                    <p class="work-menu"><span><a href="#" class="active">Websites</a>
                    </p>
                </div>
            </div>

            <div class="pro_row">
            @foreach ($projects as $project)
                <div class="col-md-4 animate-box pro_card">
                    <div class="card-body">
                        <img src="{{ asset('project-uploads/projects/'.$project->photo) }}" alt=""  class="pro_img">
                        <div class="heading">
                            <h3 class="pro_title">{{ $project->title }}</h3>
                            <p class="content"> {{ $project->description }} </p>
                        <div class=" row">
                            <div class="col-md-12">
                                <button class="btt">Detail</button>
                                <a href="https://zen-jang-259086.netlify.app/"><button class="btt">Visit site</button></a>
                                <button class="btt">Gitlab</button>
                            </div>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach

            </div>

            {{-- <div class="row">
                <div class="col-md-12 animate-box">
                    <p><a href="{{ route('port_project') }}" class="btn btn-primary btn-lg btn-load-more">Load more <i
                                class="icon-reload"></i></a>
                    </p>
                </div>
            </div> --}}
        </di>
    </section>
    {{-- contact --}}
    <section class="colorlib-contact" data-section="contact">
        <div class="colorlib-narrow-content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                    <span class="heading-meta">Get in Touch</span>
                    <h2 class="colorlib-heading">Contact</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
                        <div class="colorlib-icon">
                            <i class="icon-mail"></i>
                        </div>
                        <div class="colorlib-text">
                            <p><a href="#">rsahu8817@gmail.com</a></p>
                            <p><a href="#">rahulsahumsmd007@gmail.com</a></p>
                        </div>
                    </div>
                    <div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
                        <div class="colorlib-icon">
                            <i class="icon-phone"></i>
                        </div>
                        <div class="colorlib-text">
                            <p><a href="#">+91 8817393045</a></p>
                            <p><a href="#">+91 9399887991</a></p>
                        </div>
                    </div>
                    <div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
                        <div class="colorlib-icon">
                            <i class="icon-map"></i>
                        </div>
                        <div class="colorlib-text">
                            <p>198 West 21th Street, Suite 721 New York NY 10016</p>
                        </div>
                    </div>


                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">
                                <label>Send Message</label>
                            </div>
                            <form action="#" method="POST">
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name </label>
                                    <input type="name" class="form-control" name="name" placeholder="Enter name">
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email address</label>
                                    <input type="email" class="form-control" name="email" placeholder="Enter email">
                                </div>
                                <div class="mb-3">
                                    <label for="contact" class="form-label">contact</label>
                                    <input type="contact" class="form-control" name="contact"
                                        placeholder="Enter mobile number">
                                </div>
                                <div class="mb-3">
                                    <label for="textarea" class="form-label">Your Message</label>
                                    <textarea class="form-control" id="textarea" rows="3" name="message"></textarea>
                                </div>

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3722.3869674745306!2d82.0721070146583!3d21.097133185961376!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a28affd392a38bb%3A0x35dc75b165058eac!2sChhattisgarh%20housing%20board%20colony%2C%20mahasamund!5e0!3m2!1sen!2sin!4v1628465650456!5m2!1sen!2sin"
                width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <hr>
        </div>
    </section>

@endsection
