@extends('layouts.portfolio')

<div class="container" id="work">
    <div class="button">
        <a href="{{ route('index') }}"><button type="button" class="btn btn-outline-primary" id="back"> Back To
                Home</button></a>
    </div>
    <div class="col-md-4">
       <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2">Larry the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
    </div>
    <div class="col-md-4">
<button type="button" class="btn btn-primary">Primary</button>
<button type="button" class="btn btn-secondary">Secondary</button>
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-danger">Danger</button>
<button type="button" class="btn btn-warning">Warning</button>
<button type="button" class="btn btn-info">Info</button>
<button type="button" class="btn btn-light">Light</button>
<button type="button" class="btn btn-dark">Dark</button>

<button type="button" class="btn btn-link">Link</button>
    </div>
    <div class="col-md-4">
        <div class="animate-box" data-animate-effect="fadeInRight">
            <div class="project" style="background-image: url( {{ asset('assets/images/img-2.png') }} );">
                <div class="desc">
                    <div class="con">
                        <h3><a href="work.html">Electronic shop</a></h3>
                        <span>Website</span>
                        <p class="icon">
                            <span><a href="#"><i class="icon-share3"></i></a></span>
                            <span><a href="#"><i class="icon-eye"></i> 100</a></span>
                            <span><a href="#"><i class="icon-heart"></i> 49</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="animate-box" data-animate-effect="fadeInRight">
            <div class="project" style="background-image: url( {{ asset('assets/images/img-2.png') }} );">
                <div class="desc">
                    <div class="con">
                        <h3><a href="work.html">Electronic shop</a></h3>
                        <span>Website</span>
                        <p class="icon">
                            <span><a href="#"><i class="icon-share3"></i></a></span>
                            <span><a href="#"><i class="icon-eye"></i> 100</a></span>
                            <span><a href="#"><i class="icon-heart"></i> 49</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="animate-box" data-animate-effect="fadeInRight">
            <div class="project" style="background-image: url( {{ asset('assets/images/img-2.png') }} );">
                <div class="desc">
                    <div class="con">
                        <h3><a href="work.html">Electronic shop</a></h3>
                        <span>Website</span>
                        <p class="icon">
                            <span><a href="#"><i class="icon-share3"></i></a></span>
                            <span><a href="#"><i class="icon-eye"></i> 100</a></span>
                            <span><a href="#"><i class="icon-heart"></i> 49</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="animate-box" data-animate-effect="fadeInRight">
            <div class="project" style="background-image: url( {{ asset('assets/images/img-2.png') }} );">
                <div class="desc">
                    <div class="con">
                        <h3><a href="work.html">Electronic shop</a></h3>
                        <span>Website</span>
                        <p class="icon">
                            <span><a href="#"><i class="icon-share3"></i></a></span>
                            <span><a href="#"><i class="icon-eye"></i> 100</a></span>
                            <span><a href="#"><i class="icon-heart"></i> 49</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
