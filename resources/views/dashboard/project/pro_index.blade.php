@extends('layouts.dashboard')

@section('title')
    Users
@endsection

@section('css')
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users @lang('app.hello')</h2>
            <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Home</a>
            </li>
            <li class="active">
                <strong>Users</strong>
            </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('projects.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add User</a>
        </div>
    </div>
@endsection

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Users List
                        </h5>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover users-table">
                                <thead>
                                    <tr>
                                        <th>Photo</th>
                                        <th>Title</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($projects as $project)
                                        <tr>
                                            <td>
                                                <img src="{{ asset('project-uploads/projects/'.$project->photo) }}" alt="" height="60" width="60">
                                            </td>
                                            <td>{{ $project->title }}</td>
                                            <td>
                                                @if ($project->status == 'active')
                                                    <span class="label label-primary">Active</span>
                                                @else
                                                    <span class="label label-danger">In-Active</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('projects.show', $project->id) }}"
                                                        class="btn-white btn btn-xs">View</a>

                                                    <a href="{{ route('projects.edit', $project->id) }}"
                                                        class="btn-white btn btn-xs">Edit</a>

                                                    <a href="javascript:;" data-project-id="{{ $project->id }}"
                                                        class="btn-white btn btn-xs delete-project">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.users-table').DataTable();

            @if (session('success'))
                toastr.success('{{ session('success') }}', 'Success');
            @endif

            $('.delete-project').click(function() {

                let userId = $(this).data('project-id');

                // Generate Token method 1
                // let token = $("meta[name='csrf-token']").attr("content");

                // Generate Token method 2
                let token = '{{ csrf_token() }}';

                let url = '{{ route('projects.destroy', ':id') }}';
                url = url.replace(':id', userId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function() {

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',

                        },
                        success: function() {
                            swal("Deleted!", "User account has been deleted.",
                                "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });

                });
            });

        });
    </script>
@endsection
