@extends('layouts.dashboard')

@section('title')
    {{-- {{ $user->name }} --}}
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection


@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Home</a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}">Users</a>
                </li>
                <li class="active">
                    {{-- <strong>{{ $user->name }}</strong> --}}
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <button class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save User</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('projects.show', $project->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-9">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>Personal Details</h3>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group @error('title') has-error @enderror">
                                                <label for="title">Title <span class="text-danger">*</span></label>
                                                <input type="text" placeholder="Enter title" class="form-control" name="title" id="title" value="{{$project->title }}">
                                                @error('title')
                                                    <span class="text-danger"> {{ $message }} </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group @error('status') has-error @enderror">
                                            <label for="status"> Status <span class="text-danger">*</span></label>
                                            <select class="form-control" name="status" id="status">
                                                <option>Select</option>
                                                <option value="active" {{ $project->status == 'active' ? 'selected' : '' }}>Active</option>
                                                <option value="inactive" {{ $project->status == 'inactive' ? 'selected' : '' }}>Inactive</option>
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group @error('description') has-error @enderror">
                                                <label for="description">Description <span class="text-danger">*</span></label>
                                                <input type="description" class="form-control" placeholder="Enter description" name="description" id="description" value="{{ $project->description }}">
                                                @error('description')
                                                    <span class="text-danger"> {{ $message }} </span>
                                                @enderror
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                </div>

                    <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Uploaded Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file" name="file" data-default-file="{{ asset('project-uploads/projects/'.$project->photo) }}"/>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $('.dropify').dropify();

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $(document).on('click', '#save-data', function(){
            $('form').submit();
        });


    </script>

@endsection

