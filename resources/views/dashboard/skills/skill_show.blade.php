@extends('layouts.dashboard')

@section('title')
    Add User
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">

        </div>
        <div class="col-lg-2">
            <a href="{{ route('skills.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            {{-- <form action="{{ route('skills.show',$skill->id) }}" method="post"> --}}
            <form action="{{ route('skills.show', $skill->id) }}" method="post" enctype="multipart/form-data">

                @csrf

                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Personal Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name">Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter name" class="form-control" name="name"
                                            id="name" value="{{ $skill->name }}">
                                        @error('name')
                                            <span class=" text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                    <div class="col-lg-3">
                                         <div class="ibox float-e-margins">
                                          <div class="ibox-title">
                                          <h5>Uploaded Photo</h5>
                                            </div>
                                        <div class="ibox-content">
                                     <input type="file" class="dropify" name="file" data-default-file="{{ asset('skill_uploads/skills/'.$skill->image) }}" />
                                    </div>
                                 </div>
                        </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('Status') has-error @enderror">
                                        <label for="Status">Status <span class="text-danger">*</span></label>
                                        <select class="custom-select" name="status">
                                            <option selected>Status</option>
                                            <option value="1" {{ $skill->status =='active'?'selected':''}}>Active</option>
                                            <option value="2" {{ $skill->status =='inactive'?'selected':''}}>In Active</option>
                                          </select>
                                        @error('Status')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $('.dropify').dropify();

        $('.datepicker').datepicker({
            // format: 'yyyy-mm-dd',
            format: 'dd-mm-yyyy',
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        })
    </script>

@endsection



