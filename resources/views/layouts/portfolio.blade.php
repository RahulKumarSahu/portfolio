<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Rahul Portfolio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />


    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="{{ asset('assets/favicon.icon') }}">
    <link href=" {{ asset('assets/https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700') }}"
        rel="stylesheet">
    <link href="{{ asset('assets/https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700') }}"
        rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('assets/css/icomoon.css') }}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="{{ asset('assets/css/css/flexslider.css') }}">
    <!-- Flaticons  -->
    <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon/font/flaticon.css') }}">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
    <!-- Theme style  -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/custome.css') }}">
    {{-- bootstrap --}}

    @yield('bootstrap')
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script> --}}

    <!-- Modernizr JS -->
    <script src=" {{ asset('assets/js/modernizr-2.6.2.min.js') }}"></script>


</head>

<body>

    <div id="colorlib-page">
        {{-- side-navbar --}}

        <div class="container-fluid">
            @yield('sidebar')
            {{-- main --}}
            <div id="colorlib-main">
                @yield('main')

                <!-- jQuery -->
                <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
                <!-- jQuery Easing -->
                <script src="{{ asset('assets/js/jquery.easing.1.3.js') }}"></script>
                <!-- Bootstrap -->
                <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
                <!-- Waypoints -->
                <script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
                <!-- Flexslider -->
                <script src="{{ asset('assets/js/jquery.flexslider-min.js') }}"></script>
                <!-- Owl carousel -->
                <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
                <!-- Counters -->
                <script src="{{ asset('assets/js/jquery.countTo.js') }}"></script>

                <!-- MAIN JS -->
                <script src="{{ asset('assets/js/main.js') }}"></script>
            </div>

        </div>
    </div>
</body>

</html>
