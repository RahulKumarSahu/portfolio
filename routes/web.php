<?php

use App\Http\Controllers\ExperienceController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\SkillController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return redirect('/index');
});

// frontend
Route::get('index', [FrontController::class, 'index'])->name('index');
Route::get('port_project', [FrontController::class, 'port_project'])->name('port_project');


// experiences
Route::resource('projects', ProjectController::class);

Route::resource('experiences', ExperienceController::class);

//skills
Route::resource('skills', SkillController::class);


Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', function () {
        return view('dashboard.dashboard.index');
    })->name('dashboard');

    // User CRUD
    Route::resource('users', UserController::class);
    Route::resource('projects', ProjectController::class);
});
